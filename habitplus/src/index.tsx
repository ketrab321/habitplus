import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorkerRegistration from "./serviceWorkerRegistration";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import store from "./store";

import firebase from "firebase/app";
import "firebase/analytics";
import { askForPermissionToReceiveNotifications } from "./push-notification";

const firebaseConfig = {
  apiKey: "AIzaSyDHcszqg45IjPRQA21lFRqoKESlxarEfGo",
  authDomain: "habitplus-h481t.firebaseapp.com",
  projectId: "habitplus-h481t",
  storageBucket: "habitplus-h481t.appspot.com",
  messagingSenderId: "138050873374",
  appId: "1:138050873374:web:ea9b187ccc43617c5e260f",
  measurementId: "G-QRVW8QEW4R",
  databaseURL:
    "https://habitplus-h481t-default-rtdb.europe-west1.firebasedatabase.app/",
  // storageBucket: "gs://habitplus-h481t.appspot.com/"  // storage for media files
};
const firebaseApp = firebase.apps.length
  ? firebase.app()
  : firebase.initializeApp(firebaseConfig);

askForPermissionToReceiveNotifications();

ReactDOM.render(
  <Provider store={store}>
    <App firebase={firebaseApp} />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
