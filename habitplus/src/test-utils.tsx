// test-utils.ts
import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
// Import your own reducers
import reducers from './reducers'
import thunk from 'redux-thunk'

// Reimplementation of render that allows to bind redux state
function render(
  ui: React.ReactElement,
  {
    initialState = {},
    store = createStore(reducers, initialState, applyMiddleware(thunk)),
    ...renderOptions
  }: { initialState?: any, store?: any } = {}
) {
  function Wrapper({ children }: {children?: React.ReactNode}) {
    return <Provider store={store}>{children}</Provider>
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

// re-export everything
export * from '@testing-library/react'
// override render method
export { render }

