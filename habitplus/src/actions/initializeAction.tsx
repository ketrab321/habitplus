import firebase from "firebase/app";
import { ThunkAction } from "redux-thunk";
import { HabitsActions, setHabits } from './habitsActions';
import { setTags, TagsActions } from "./tagsActions";
import { setTemplates, TemplatesActions } from "./templateActions";

type Actions = HabitsActions | TagsActions | TemplatesActions

type ThunkResult<R> = ThunkAction<R, undefined, undefined, Actions>

function mapHistoryTags(tags: any){
    let tagsIds: string[] = []
    if(tags){
        tagsIds = Object.values(tags);
    }
    return tagsIds;
}

function mapHabitHistory(history: any){
    if(history){
        let mapedHistory = Object.keys(history).reverse().map(historyKey => {
            let tags = history[historyKey]["tags"];
            return {
                ...history[historyKey],
                id: historyKey,
                tags: mapHistoryTags(tags)
            }
        })
        return mapedHistory;
    }
    return undefined
}

export default function initialize(): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser!;
    
        // Reading habits data
        const habitsRef = firebase.database().ref('habits/' + user.uid);

        const asyncHabits = habitsRef.get().then(async (snapshot) => {
            const data = snapshot.val();
            if(data){
                let habitsPromises = Object.keys(data).map(async key => {     
                    const historyRef = firebase.database().ref('history/' + user.uid + '/' + key);  
                    const history = (await historyRef.get()).val();            
                    return {
                        ...data[key],
                        id: key,
                        history: mapHabitHistory(history)
                    }
                })
                const habits = await Promise.all(habitsPromises);
                dispatch(setHabits(habits));
            }
        })

        await asyncHabits;

        // Reading tags data
        const tagsRef = firebase.database().ref('tags/' + user.uid);
        tagsRef.get().then((snapshot) => {
            const data = snapshot.val();
            if(data) {
                const tags = Object.keys(data).map(key => {
                    return {
                        ...data[key],
                        id: key
                    }
                })
                dispatch(setTags(tags));
            }
        })

        //Reading templates data
        const templatesRef = firebase.database().ref('templates/');
        templatesRef.get().then((snapshot) => {
            const data = snapshot.val();
            if(data) {
                const templates = Object.keys(data).map(key => {
                    return {
                        ...data[key],
                        id: key
                    }
                });
                const publicTemplates = templates.filter(e => e.type === 'public');
                dispatch(setTemplates(publicTemplates));
            }
        })

        // Reading posts data
        // const postsRef = firebase.database().ref('posts/' + user.uid);
    }
}