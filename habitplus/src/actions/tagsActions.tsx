import { ThunkAction } from "redux-thunk"
import { TagsType, TagType } from '../reducers/TagsReducers'
import firebase from "firebase/app";

type ThunkResult<R> = ThunkAction<R, TagsType, undefined, TagsActions>

export function createNewTag(tagLabel: string, tagIcon: string, habitId: string, historyId: string): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const tagsRef = firebase.database().ref('tags/' + user.uid);
            const tagId = (await tagsRef.push({
                label: tagLabel,
                icon: tagIcon
            })).key!

            await dispatch({
                type: "CREATE_NEW_TAG",
                payload: {
                    label: tagLabel,
                    icon: tagIcon,
                    id: tagId
                }
            })
            await addTagToProgressComment(habitId, historyId, tagId)(dispatch, getState, undefined);
        }
    }
}

export function editTag(tag: TagType): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const tagsRef = firebase.database().ref('tags/' + user.uid + '/' + tag.id);
            await tagsRef.set({
                label: tag.label,
                icon: tag.icon
            })

            await dispatch({
                type: "EDIT_TAG",
                payload: tag
            })
        }
    }
}

export function setTags(tags: TagsType): SetTagsActionType {
    return {
        type: "SET_TAGS",
        payload: tags
    }
}


export function addTagToProgressComment(habitId: string, historyId: string, tagId: string): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const historyRef = firebase.database().ref('history/' + user.uid + '/' + habitId + '/' + historyId + '/tags');
            historyRef.push(tagId);
        }

        dispatch({
            type: "ADD_TAG_TO_HISTORY",
            payload: {
                habitId,
                historyId,
                tagId
            }
        })
    }
}

export type SetTagsActionType = {
    type: "SET_TAGS",
    payload: TagsType
}

export type CreateTagActionType = {
    type: "CREATE_NEW_TAG",
    payload: TagType
}

export type EditTagActionType = {
    type: "EDIT_TAG",
    payload: TagType
}

export type AddTagActionType = {
    type: "ADD_TAG_TO_HISTORY",
    payload: {
        habitId: string,
        historyId: string,
        tagId: string
    }
}

export type TagsActions = CreateTagActionType | AddTagActionType | EditTagActionType | SetTagsActionType; // Later add more action types

