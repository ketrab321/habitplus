
import { ThunkAction } from 'redux-thunk';
import firebase from "firebase/app";
import 'firebase/auth';
import 'firebase/database';


export type UserProfile = firebase.User

export type UserAction = {
    type: string,
    payload: UserProfile
}

export type SignInAction = {
    type: string,
    payload: UserProfile | null
}

type SignOutAction = {
    type: string
}

type Actions = UserAction | SignInAction | SignOutAction;
type ThunkResult<R> = ThunkAction<R, any, undefined, Actions>

export const updateProfile = (newUsername: string): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser!;
        await user.updateProfile({
            displayName: newUsername
        });

        dispatch({
            type: 'USER_UPDATE',
            payload: user
        })
    }
}

export const signIn = (): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;

        dispatch({
            type: 'SIGN_IN',
            payload: user
        });
    }
}

export const signOut = (): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        await firebase.auth().signOut();

        dispatch({
            type: 'SIGN_OUT',
        });
    }
}

export const deleteAccount = (): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser!;
        const userId = user.uid;
        // Deleting an account requires reauthentication
        switch(user.providerData[0]?.providerId) {
            case firebase.auth.GoogleAuthProvider.PROVIDER_ID:
                await user.reauthenticateWithPopup(new firebase.auth.GoogleAuthProvider());
                break;
            // case firebase.auth.EmailAuthProvider.PROVIDER_ID:
            //     const userPassword = ""; // TODO: we need to ask user for password somehow
            //     const credential = firebase.auth.EmailAuthProvider.credential( user.email!,  userPassword);
            //     await user.reauthenticateWithCredential(credential);
            //     break;
            default:
                console.log("Unsupported provider");    
        }

        await firebase.database().ref().child('users').child(userId).remove();
        await firebase.auth().currentUser!.delete();

        dispatch({
            type: 'SIGN_OUT',
        });
    }
}
