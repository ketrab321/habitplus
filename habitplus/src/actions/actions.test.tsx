import React from 'react';

import * as actions from './index';

describe('actions', () => {
    it('should create an action to change displayed tab index', () => {
        const index = 1;
        const expectedAction =  {
            type: "CHANGE_TAB_INDEX",
            payload: index
        }

        expect(actions.changeTabIndex(index)).toEqual(expectedAction)
    })
})