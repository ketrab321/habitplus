import type {Color} from '@material-ui/lab/Alert';


/**
 * Opens snackbar in the app. Snackbar is closed automatically.
 * @param message Message to be shown in snackbar.
 * @param severity Must be one of: "info", "success", "warning", "error"
 */
export const openSnackbar = (message: string, severity: Color = "info"): OpenSnackbarActionType => {
    return {
        type: "OPEN_SNACKBAR",
        payload: {
            message: message,
            severity: severity
        }
    }
}

/**
 * Closes snackbar. You don't have to call this action.
 * Every snackbar is closed automatically after some time.
 */
 export const closeSnackbar = () => {
    return {
        type: "CLOSE_SNACKBAR"
    }
}


export type OpenSnackbarActionType = {
    type: "OPEN_SNACKBAR",
    payload: {
        message: string,
        severity: string
    }
}
