import { ThunkAction } from 'redux-thunk';
import { TemplatesType, TemplateType } from '../reducers/TemplatesReducers';
import { RootState } from "../store";
import firebase from "firebase/app";
import { openSnackbar, OpenSnackbarActionType } from './snackbarActions';
import { HabitType } from '../reducers/HabitsReducers';
import { addHabit } from './habitsActions';

type ThunkResult<R> = ThunkAction<R, RootState, undefined, TemplatesActions>


export const addTemplate = (template: TemplateType): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const templatesRef = firebase.database().ref('templates/');
            template.id = (await templatesRef.push(template)).key!;

            dispatch(openSnackbar("Template has been added", "success"));
        }
        
        dispatch({
            type: 'ADD_TEMPLATE',
            payload: template
        })
    }
}

export const useTemplate = (template: TemplateType): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const habit: HabitType = {
            id: '',
            title: template.title,
            description: template.description,
            startTime: new Date(Date.now()).toISOString(),
            history: [],
            icon: template.icon
        }

        dispatch(openSnackbar("Added habit from the template", "info"));

        addHabit(habit)(dispatch, getState, undefined);
    }
}

export const likeTemplate = (template: TemplateType): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const templateRef = await firebase.database().ref('templates/' + template.id);
        template.likes += 1;
        template.voted += "," + firebase.auth().currentUser!.uid!;
        templateRef.update({likes: template.likes, voted: template.voted});

        dispatch({
            type: 'UPDATE_TEMPLATE',
            payload: template
        });

        dispatch(openSnackbar("Your vote has been added", "success"));
    }

}

export const unlikeTemplate = (template: TemplateType): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const templateRef = await firebase.database().ref('templates/' + template.id);
        template.likes -= 1;
        const votedList = template.voted.split(',').filter(e => e !== firebase.auth().currentUser!.uid!)
        template.voted = votedList.join(',');
        templateRef.update({likes: template.likes, voted: template.voted});

        dispatch({
            type: 'UPDATE_TEMPLATE',
            payload: template
        });

        dispatch(openSnackbar("Like removed", "info"));
    }

}

export const removeTemplate = (template: TemplateType): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const templateRef = await firebase.database().ref('templates/' + template.id);
        await templateRef.remove();

        dispatch({
            type: 'REMOVE_TEMPLATE',
            payload: template
        });

        dispatch(openSnackbar("Template removed", "info"));
    }

}

export const setTemplateType = (template: TemplateType, newType: 'public' | 'private'): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const templateRef = await firebase.database().ref('templates/' + template.id);
        template.type = newType;
        templateRef.update({type: template.type});

        dispatch({
            type: 'UPDATE_TEMPLATE',
            payload: template
        });
    }

}

export function setTemplates(tags: TemplatesType): SetTemplatesActionType {
    return {
        type: "SET_TEMPLATES",
        payload: tags
    }
}

export type SetTemplatesActionType = {
    type: "SET_TEMPLATES",
    payload: TemplatesType
}
export type UpdateTemplatesActionType = {
    type: "UPDATE_TEMPLATE",
    payload: TemplateType
}
export type AddTemplateActionType = {
    type: "ADD_TEMPLATE",
    payload: TemplateType
}
export type RemoveTemplateActionType = {
    type: "REMOVE_TEMPLATE",
    payload: TemplateType
}

export type TemplatesActions = AddTemplateActionType |
                              RemoveTemplateActionType |
                              OpenSnackbarActionType |
                              SetTemplatesActionType |
                              UpdateTemplatesActionType; // Later add more action types

