import { ThunkAction } from "redux-thunk"
import { HabitType, HabitsType, HabitProgressComment } from '../reducers/HabitsReducers'
import firebase from "firebase/app";
import { RootState } from "../store";
import { addTemplate } from "./templateActions"
import { TemplateType } from "../reducers/TemplatesReducers";


type ThunkResult<R> = ThunkAction<R, RootState, undefined, HabitsActions>

export const addHabit = (habit: HabitType): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const habitsRef = firebase.database().ref('habits/' + user.uid);
            habit.id = (await habitsRef.push({
                description: habit.description,
                icon: habit.icon,
                startTime: habit.startTime,
                title: habit.title
            })).key!
            await dispatch({
                type: 'NEW_HABIT',
                payload: 
                {
                    ...habit, 
                }
            });
            addHabitProgressComment(habit.id, 
                {
                  type: "success",
                  time: new Date(Date.now()).toISOString(),
                  comment: "I started a new habit",
                }
            )(dispatch, getState, undefined);
        }
    }
}

export const updateHabit = (habit: HabitType, id: string): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;

        if(user){
            const habitsRef = firebase.database().ref('habits/' + user.uid + '/' + id + '/');
            await habitsRef.update({
                description: habit.description,
                icon: habit.icon,
                startTime: habit.startTime,
                title: habit.title
            })
            await dispatch({
                type: 'UPDATE_HABIT',
                payload: 
                {
                    ...habit, 
                    id: id
                }
            });
            addHabitProgressComment(habit.id, 
                {
                  type: "success",
                  time: new Date(Date.now()).toISOString(),
                  comment: "I updated my habit :)",
                }
            )(dispatch, getState, undefined);
        }
    }
}

export const removeHabit = (id: string): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;

        if(user){
            const habitsRef = firebase.database().ref('habits/' + user.uid + '/' + id + '/');
            await habitsRef.remove();
            const historyRef = firebase.database().ref('history/' + user.uid + '/' + id + '/');
            await historyRef.remove();
            await dispatch({
                type: 'REMOVE_HABIT',
                payload: 
                {
                    id: id
                }
            });
        }
    }
}


type NewHabitProgressComment = {
    type: 'success' | 'fail';
    time: string;
    comment: string;
    tags?: string[] | undefined;
}

export const addHabitProgressComment = (habitId: string, habitProgressComment: NewHabitProgressComment): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const habitsProgressHistoryRef = firebase.database().ref('history/' + user.uid + '/' + habitId + '/');
            (habitProgressComment as HabitProgressComment).id = (await habitsProgressHistoryRef.push(habitProgressComment)).key!;
        }
        
        dispatch({
            type: 'NEW_HABIT_PROGRESS_COMMENT',
            payload: 
            {
                habitId,
                progressComment: (habitProgressComment as HabitProgressComment)
            }
        })
    }
}

export const editHabitProgressComment = (habitId: string, commentId: string, habitProgressComment: NewHabitProgressComment): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const habitsProgressHistoryRef = firebase.database().ref('history/' + user.uid + '/' + habitId + '/' + commentId);
            habitsProgressHistoryRef.update(habitProgressComment)
        }
        
        dispatch({
            type: 'EDIT_HABIT_PROGRESS_COMMENT',
            payload: 
            {
                habitId,
                commentId,
                progressComment: (habitProgressComment as HabitProgressComment)
            }
        })
    }
}

export const removeHabitProgressComment = (habitId: string, commentId: string): ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const user = firebase.auth().currentUser;
        
        if(user){
            const habitsProgressHistoryRef = firebase.database().ref('history/' + user.uid + '/' + habitId + '/' + commentId);
            habitsProgressHistoryRef.remove();
        }
        
        dispatch({
            type: 'REMOVE_HABIT_PROGRESS_COMMENT',
            payload: 
            {
                habitId,
                commentId,
            }
        })
    }
}

export const saveHabitAsTemplate = (habit: HabitType): ThunkResult<Promise<void>> => {

    const user = firebase.auth().currentUser!;
        
    const template: TemplateType = {
        id: null,
        type: 'private',
        icon: habit.icon,
        authorName: user.displayName!,
        authorId: user.uid!,
        title: habit.title,
        description: habit.description,
        likes: 0,
        voted: ''
    }

    return addTemplate(template);
}



export const setHabits = (habits: HabitsType): SetHabitsActionType => {
    return {
        type: 'SET_HABITS',
        payload: habits
    }
}


export type SetHabitsActionType = {
    type: "SET_HABITS",
    payload: HabitsType
}

export type NewHabitActionType = {
    type: "NEW_HABIT",
    payload: HabitType
}

export type UpdateHabitActionType = {
    type: "UPDATE_HABIT",
    payload: HabitType
}

export type RemoveHabitActionType = {
    type: "REMOVE_HABIT",
    payload: {id: string}
}

export type NewHabitProgressCommentActionType = {
    type: "NEW_HABIT_PROGRESS_COMMENT",
    payload: {
        habitId: string,
        progressComment: HabitProgressComment
    }
}

export type EditHabitProgressCommentActionType = {
    type: "EDIT_HABIT_PROGRESS_COMMENT",
    payload: {
        habitId: string,
        commentId: string,
        progressComment: HabitProgressComment
    }
}

export type RemoveHabitProgressCommentActionType = {
    type: "REMOVE_HABIT_PROGRESS_COMMENT",
    payload: {
        habitId: string,
        commentId: string,
    }
}

export type HabitsActions = NewHabitActionType | 
                            UpdateHabitActionType | 
                            RemoveHabitActionType |
                            SetHabitsActionType | 
                            NewHabitProgressCommentActionType | 
                            EditHabitProgressCommentActionType |
                            RemoveHabitProgressCommentActionType; // Later add more action types

