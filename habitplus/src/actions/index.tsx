export * from './habitsActions';
export * from './userActions';
export * from './snackbarActions';


export const changeTabIndex = (index: number) => {
    return {
        type: "CHANGE_TAB_INDEX",
        payload: index
    }
}


