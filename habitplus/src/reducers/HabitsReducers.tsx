import { HabitsActions } from "../actions/habitsActions";
import { TagsActions, AddTagActionType } from "../actions/tagsActions";

export type HabitProgressComment = {
  type: "success" | "fail";
  time: string;
  comment: string;
  id: string;
  tags: Array<string>;
};

export type HabitType = {
  id: string;
  title: string;
  description: string;
  icon: {
    type: string;
    color: string;
    textColor: string;
  };
  startTime: string;
  history: Array<HabitProgressComment>;
};

export type HabitsType = Array<HabitType>;

type Actions = HabitsActions | TagsActions;

export function HabitReducer(
  habits: HabitsType = [],
  action: Actions
): HabitsType {
  if (action.type === "NEW_HABIT") {
    const newHabits = [action.payload, ...habits];
    return newHabits;
  } else if (action.type === "UPDATE_HABIT") {
    const updateHabits = habits.map((habit) => {
      if (habit.id === action.payload.id) {
        return action.payload;
      } else {
        return habit;
      }
    });

    return updateHabits;
  } else if (action.type === "REMOVE_HABIT") {
    return habits.filter((habit) => habit.id !== action.payload.id);
  } else if (action.type === "SET_HABITS") {
    const newHabits = action.payload;
    return newHabits;
  } else if (action.type === "NEW_HABIT_PROGRESS_COMMENT") {
    const newHabits = habits.map((habit) => {
      if (habit.id === action.payload.habitId) {
        const newHistory = habit.history
          ? [action.payload.progressComment, ...habit!.history]
          : [action.payload.progressComment];
        const newHabit = {
          ...habit,
          history: newHistory,
        };
        return newHabit;
      } else {
        return habit;
      }
    });
    return newHabits;
  } else if (action.type === "EDIT_HABIT_PROGRESS_COMMENT") {
    const newHabits = habits.map((habit) => {
      if (habit.id === action.payload.habitId) {
        habit.history = habit.history.map((comment) => {
          if (comment.id === action.payload.commentId) {
            return action.payload.progressComment;
          }
          return comment;
        });
        return habit;
      } else {
        return habit;
      }
    });
    return newHabits;
  } else if (action.type === "REMOVE_HABIT_PROGRESS_COMMENT") {
    const newHabits = habits.map((habit) => {
      if (habit.id === action.payload.habitId) {
        habit.history = habit.history.filter((comment) => {
          return comment.id !== action.payload.commentId;
        });
        return habit;
      } else {
        return habit;
      }
    });
    return newHabits;
  } else if (action.type === "ADD_TAG_TO_HISTORY") {
    const castedAction = action as AddTagActionType;
    return mapHabits(
      habits,
      castedAction.payload.habitId,
      castedAction.payload.historyId,
      castedAction.payload.tagId
    );
  }
  return habits;
}

function mapHabits(
  habits: HabitsType,
  habitId: string,
  historyId: string,
  newTagId: string
) {
  return habits.map((habit) => {
    if (habit.id === habitId) {
      let newHistory: Array<HabitProgressComment> = [];
      if (habit.history) {
        newHistory = mapHabitHistory(habit.history, historyId, newTagId);
      }
      const newHabit = {
        ...habit,
        history: newHistory,
      };
      return newHabit;
    } else {
      return habit;
    }
  });
}

function mapHabitHistory(
  history: Array<HabitProgressComment>,
  historyId: string,
  newTagId: string
) {
  return history.map((comment) => {
    if (comment.id === historyId) {
      return mapTags(comment, newTagId);
    }
    return comment;
  });
}

function mapTags(comment: HabitProgressComment, newTagId: string) {
  let newTags = [newTagId];
  if (comment.tags) {
    if (!comment.tags.includes(newTagId)) {
      newTags = [newTagId, ...comment.tags];
    } else {
      newTags = comment.tags;
    }
  }
  return {
    ...comment,
    tags: newTags,
  };
}
