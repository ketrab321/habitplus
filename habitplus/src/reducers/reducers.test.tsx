import React from "react";

import { TabIndexReducer } from "./NavigationReducers";

describe("Tab Index Reducer", () => {
  it("Should return initial tab number, which is 0", () => {
    expect(TabIndexReducer(undefined, { type: "", payload: 999 })).toEqual(0);
  });

  it("Should return tab index 2", () => {
    expect(
      TabIndexReducer(0, { type: "CHANGE_TAB_INDEX", payload: 2 })
    ).toEqual(2);
  });
});
