import {
  AddTemplateActionType,
  TemplatesActions,
} from "../actions/templateActions";

export type TemplateType = {
  type: "public" | "private";
  authorName: string;
  authorId: string;
  id: string | null;
  icon: {
    type: string;
    color: string;
    textColor: string;
  };
  title: string;
  description: string;
  likes: number;
  voted: string;
};

export type TemplatesType = Array<TemplateType>;

export function TemplatesReducer(
  templates: TemplatesType = [],
  action: TemplatesActions
) {
  if (action.type === "ADD_TEMPLATE") {
    return [(action as AddTemplateActionType).payload, ...templates];
  } else if (action.type === "REMOVE_TEMPLATE") {
    return templates.filter((template) => template.id !== action.payload.id);
  } else if (action.type === "UPDATE_TEMPLATE") {
    const newTemplates = templates.map((template) => {
      if (template.id === action.payload.id) {
        return action.payload;
      } else {
        return template;
      }
    });
    return newTemplates;
  } else if (action.type === "SET_TEMPLATES") {
    return action.payload;
  }
  return templates;
}
