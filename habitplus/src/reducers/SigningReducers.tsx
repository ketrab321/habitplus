import { SignInAction } from "../actions/userActions";

export const SigningReducer = (signedIn = null, action: SignInAction) => {
  switch (action.type) {
    case "SIGN_IN":
      return true;
    case "SIGN_OUT":
      return false;
    default:
      return signedIn;
  }
};
