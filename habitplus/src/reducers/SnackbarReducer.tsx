import type { Color } from "@material-ui/lab/Alert";

type SnackbarState = {
  open: boolean;
  message: string;
  severity: Color;
};

type SnackbarAction = {
  type: string;
  payload: {
    message: string;
    severity: Color;
  };
};

export const SnackbarReducer = (
  snackbar: SnackbarState = {
    open: false,
    message: "",
    severity: "info",
  },
  action: SnackbarAction
) => {
  switch (action.type) {
    case "OPEN_SNACKBAR":
      return {
        open: true,
        message: action.payload.message,
        severity: action.payload.severity,
      };
    case "CLOSE_SNACKBAR":
      return {
        open: false,
        message: snackbar.message,
        severity: snackbar.severity,
      };
    case "USER_UPDATE":
      return {
        open: true,
        message: "User profile has been updated",
        severity: "success",
      };
    default:
      return snackbar;
  }
};
