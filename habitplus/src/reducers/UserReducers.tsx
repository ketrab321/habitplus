import { UserProfile, UserAction } from "../actions/userActions";

export const UserProfileData = (
  user: UserProfile | null = null,
  action: UserAction
) => {
  switch (action.type) {
    case "USER_UPDATE":
    case "SIGN_IN":
      return { ...action.payload };
    case "SIGN_OUT":
      return null;

    default:
      return user;
  }
};
