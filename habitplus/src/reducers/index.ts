import { combineReducers } from 'redux';

import { TabIndexReducer } from './NavigationReducers'
import { SigningReducer } from './SigningReducers'
import { UserProfileData } from './UserReducers'
import { HabitReducer } from './HabitsReducers'
import { TemplatesReducer } from './TemplatesReducers'
import { SnackbarReducer } from './SnackbarReducer';
import { TagsReducer } from './TagsReducers';

export default combineReducers({
    // Here combine reducers from other files
    tabIndex: TabIndexReducer,
    signedIn: SigningReducer,
    user: UserProfileData,
    habits: HabitReducer,
    snackbar: SnackbarReducer,
    tags: TagsReducer,
    templates: TemplatesReducer
});
