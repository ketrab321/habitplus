import {
  TagsActions,
  CreateTagActionType,
  SetTagsActionType,
} from "../actions/tagsActions";

export type TagType = {
  label: string;
  icon: string;
  id: string;
};

export type TagsType = Array<TagType>;

export function TagsReducer(tags: TagsType = [], action: TagsActions) {
  if (action.type === "CREATE_NEW_TAG") {
    return [(action as CreateTagActionType).payload, ...tags];
  } else if (action.type === "EDIT_TAG") {
    const newTags = tags.map((tag) => {
      if (tag.id === action.payload.id) {
        return action.payload;
      } else {
        return tag;
      }
    });
    return newTags;
  } else if (action.type === "SET_TAGS") {
    return (action as SetTagsActionType).payload;
  }
  return tags;
}
