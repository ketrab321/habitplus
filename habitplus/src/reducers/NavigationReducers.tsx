type Action = {
  type: string;
  payload: any;
};

export const TabIndexReducer = (index = 0, action: Action) => {
  if (action.type === "CHANGE_TAB_INDEX") {
    return action.payload;
  } else {
    return index;
  }
};
