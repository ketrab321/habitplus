import { Avatar, Box, Grid, IconButton, Paper } from "@material-ui/core";
import { makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import IconHelper from "./IconHelper";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grid: {
        maxWidth: "193px",
        minwidth: "97px",
    },
    gridContainer: {
        maxHeight: "25vh",
        minHeight: "73px",
        maxWidth: "216px",
        minwWidth: "120px",
        overflowY: "scroll",
    },
    iconPicker: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    selectedIconCircle: {
        maxHeight: "73px",
        minHeight: "48px",
        maxWidth: "73px",
        minWidth: "48px",
        marginRight: "3vw",
    },
    selectedIcon: {
        fontSize: "30px",
    }
  }),
);

type IconPickerParams = {
    onSelectIcon: (value: string) => void,
    iconType?: string,
    color?: string,
    textColor?: string
}

export default function IconPicker(params: IconPickerParams){
    const classes = useStyles();

    let initialType = params.iconType ? params.iconType : "help";
    let icon = initialType;
    const setIcon = (type: string) => {
        icon = type;
    }

    const colorTheme = params.color ? params.color : "grey";
    const textColor = params.textColor ? params.textColor : "white";

    const iconClickCallback = (iconType: string) => {
        setIcon(iconType);
        params.onSelectIcon(iconType);
        return undefined;
    }

    const options = [
        "bed",
        "alarm",
        "beach",
        "moon",
        "sun",
        "brush",
        "bubbles",
        "work",
        "cake",
        "child",
        "bike",
        "run",
        "car",
        "trophy",
        "food",
        "heart",
        "fitness",
        "coffee",
        "star",
        "help",
        "laptop",
        "alcohol",
        "music",
        "people",
        "pet",
        "camera",
        "swimming",
        "learning",
        "smoking",
        "games",
        "weekend"
    ].map(iconType => {
        let selectedIconStyle = {};
        if(iconType === icon){
            selectedIconStyle = {
                "color": colorTheme
            };
        }
        return (
            <IconButton key={iconType} onClick={()=>{iconClickCallback(iconType)}}>
                <IconHelper style={selectedIconStyle} iconType={iconType}/>
            </IconButton>
        )
    })

    return (
        <Box className={classes.iconPicker}>
            <Avatar style={{ 
                "backgroundColor": colorTheme,
                "color": textColor
            }} className={classes.selectedIconCircle}>
                <IconHelper iconType={icon} className={classes.selectedIcon}/>
            </Avatar>
            <Paper variant="outlined" className={classes.gridContainer}>
                <Grid
                    className={classes.grid}
                    container
                    direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                >
                    {options}
                </Grid>
            </Paper>
        </Box>
    )
}