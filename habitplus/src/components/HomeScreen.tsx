import FullWidthTabs from './FullWidthTabs';
import Snackbar from './Snackbar';

import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import initialize from '../actions/initializeAction';
import { connect, ConnectedProps } from 'react-redux';
import React from 'react';


const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: green,
    secondary: {
      main: '#ef5350',
    },
  },
  typography: {
    allVariants: {
      color: '#ffffff'
    }
  }
});

class HomeScreen extends React.Component<ReduxProps> {
  constructor(props: ReduxProps){
    super(props);
    props.initialize();
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <FullWidthTabs></FullWidthTabs>
        </div>
        <Snackbar />
      </ThemeProvider>
    );
  }
}

const connector = connect(undefined, {
  initialize
})

type ReduxProps = ConnectedProps<typeof connector>

export default connector(HomeScreen);
