import React from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';
import * as firebaseui from 'firebaseui'
import 'firebaseui/dist/firebaseui.css'

const uiConfig: firebaseui.auth.Config = {
    callbacks: {
      signInSuccessWithAuthResult: function(authResult, redirectUrl) {
        // User successfully signed in.
        // Return type determines whether we continue the redirect automatically
        // or whether we leave that to developer to handle.

        // We could use Redux here to change state of the app.
        // Currently we use callback (see: App.tsx -> onAuthStateChanged)
        return false;
      },
      uiShown: function() {
        // When Login UI is rendered, hide the loader.
        // document.getElementById('loader').style.display = 'none';
      }
    },
    signInFlow: 'popup', // or 'redirect'
    // signInSuccessUrl: '/', // redirection
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      firebase.auth.GithubAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
      firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    tosUrl: '<your-tos-url>', // Terms of service url.
    privacyPolicyUrl: '<your-privacy-policy-url>' // Privacy policy url.
};

class LoginScreen extends React.Component {

    ui: firebaseui.auth.AuthUI

    constructor(props: any) {
        super(props);
        this.ui = firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(firebase.auth());
    }

    componentDidMount() {
        this.ui.start('#firebaseui-auth-container', uiConfig );
    }

    render() {
        return (
            <div style={{textAlign: 'center'}}>
                <h1>Habit+</h1>
                <div id="firebaseui-auth-container"></div>       
            </div>
        )
    }
}

export default LoginScreen;
