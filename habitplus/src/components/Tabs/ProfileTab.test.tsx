import { render, fireEvent, screen, queryByAttribute } from '../../test-utils'
import ProfileTab from './ProfileTab'

const getById = queryByAttribute.bind(null, 'id');


describe("Renders ProfileTab with initial state and checks if ProfileTab shows information correctly", () => {
  
  it('Checks if ProfileTab renders good, and displays all information', () => {
    const dom = render(<ProfileTab />, { initialState: { user: { displayName: "Tester", email: "test@example.com", photoURL: ""} } });

    const usernameInput = getById(dom.container, 'username') as HTMLInputElement;
    const emailInput = getById(dom.container, 'email') as HTMLInputElement;

    expect(usernameInput.value).toBe("Tester");
    expect(emailInput.value).toBe("test@example.com");
    expect(getById(dom.container, 'save-changes')).toHaveClass('Mui-disabled');

    // Changing username should unlock the save button.
    fireEvent.change(usernameInput, {target: {value: 'I need coffe'}});
    expect(getById(dom.container, 'save-changes')).not.toHaveClass('Mui-disabled');
  });

})
