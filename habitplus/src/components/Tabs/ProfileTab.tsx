import React from 'react';
import { Box, Button, Typography } from "@material-ui/core"
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import SaveIcon from '@material-ui/icons/Save';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import WarningIcon from '@material-ui/icons/Warning';
import { connect } from 'react-redux';
import { updateProfile, signOut, deleteAccount } from '../../actions';
import type {UserProfile} from '../../actions';
import Avatar from '@material-ui/core/Avatar';
import { createStyles, withStyles, WithStyles } from "@material-ui/core/styles";
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const styles = (theme: any) => createStyles({
    paper: {
        textAlign: "center",
        '& > h1': {
            padding: "0.5em"
        },
        '& input': {
            minWidth: "15em"
        }, 
        '& p': {
            textAlign: "center"
        }
    },
    avatar: {
        width: "7em",
        height: "7em",
    },
    box: {
        display: "flex",
        justifyContent: "center",
        padding: "1em"
    },
    dangerZone: {
        '& .MuiAccordionSummary-content, .MuiAccordionDetails-root': {
            justifyContent: "center"
        },
        '& .danger-zone': {
            color: "#ff7575",
            fontWeight: "bold"
        }
    },
});

interface Props extends WithStyles<typeof styles> {
    user: UserProfile,
    classes: any,
    updateProfile: Function,
    signOut: Function,
    deleteAccount: Function
}

type State = {
    isSnackbarOpen: boolean,
    newUsername: string,
    confirmDeleteOpen: boolean
};

class ProfileTab extends React.Component<Props, State> {

    photoUrl: string 

    constructor(props: Props) {
        super(props);
        this.state = {
            isSnackbarOpen: false,
            newUsername: this.props.user.displayName!,
            confirmDeleteOpen: false
        };
        this.photoUrl = props.user.photoURL!;
    }


    render() {
        const { classes } = this.props;
        return (
            <div>
            <Paper id='profile-tab' className={classes.paper}>
                <Typography component="h1" variant="h4">User Profile</Typography>
                <Box className={classes.box}>
                    <Avatar alt="Avatar" src={this.photoUrl} className={classes.avatar}/>
                </Box>
                <Box className={classes.box}>
                    <TextField 
                        id="username" 
                        label="Username" 
                        variant="outlined" 
                        autoComplete="off" 
                        defaultValue={this.props.user.displayName}
                        onChange={(e) => this.setState({newUsername: e.target.value})} 
                    />
                </Box>
                <Box className={classes.box}>
                    <TextField 
                        id="email" 
                        label="E-mail" 
                        variant="outlined" 
                        autoComplete="off" 
                        disabled={true} 
                        defaultValue={this.props.user.email!}
                    />
                </Box>
                <Divider light />
                <Box className={classes.box} style={{padding: "2em 0"}}>
                    <Button 
                        startIcon={<MeetingRoomIcon />} 
                        variant='outlined' 
                        onClick={() => this.signOut()} 
                        size="large"
                    >
                        Sign out
                    </Button>
                    &nbsp;
                    <Button
                        id="save-changes"
                        startIcon={<SaveIcon />} 
                        disabled={this.props.user.displayName === this.state.newUsername}
                        variant='contained' 
                        onClick={() => this.save()} size="large"
                    >
                        Save changes
                    </Button>
                </Box>
                <Divider light />
                <Accordion className={classes.dangerZone}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        id="danger-zone"
                        classes={{expanded: classes.test}}
                    >
                        <Typography className="danger-zone">Danger zone</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Box className={classes.box}>
                            <Button 
                                startIcon={<WarningIcon color='error'/>} 
                                variant='contained' 
                                onClick={() => this.confirmDeleteAccount()} 
                                size="large"
                            >
                                Delete account
                            </Button>
                        </Box>
                    </AccordionDetails>
                </Accordion>
            </Paper>
            <Dialog
                open={this.state.confirmDeleteOpen}
                onClose={() => this.setState({confirmDeleteOpen: false})}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Delete your account?</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        This action deletes your account along with all it's data. 
                        This action is permanent and cannot be undone. You may be asked to reauthenticate in order to delete your account.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.setState({confirmDeleteOpen: false})} color="primary" autoFocus>
                        Go back
                    </Button>
                    <Button onClick={() => this.deleteAccount()} color="secondary" >
                        Yes, delete
                    </Button>
                </DialogActions>
            </Dialog>
            </div>
        )
    }

    save() {
        this.props.updateProfile(this.state.newUsername);
    }

    confirmDeleteAccount() {
        this.setState({
            confirmDeleteOpen: true
        });
    }

    deleteAccount() {
        this.setState({confirmDeleteOpen: false});
        this.props.deleteAccount();
    }

    signOut() {
        this.props.signOut();
    }
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user
    }
  }

const connector = connect(mapStateToProps, {updateProfile, signOut, deleteAccount});
export default connector(withStyles(styles)(ProfileTab));

