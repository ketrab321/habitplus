import InspirationsCard from './InspirationsCard';
import { connect, ConnectedProps } from 'react-redux';
import React from "react";
import { RootState } from "../../../store";
import { Typography } from '@material-ui/core';
import { createStyles, withStyles, WithStyles } from "@material-ui/core/styles";

const styles = () => createStyles({
  root: {
      textAlign: "center"
  },
});



type Props = WithStyles<typeof styles> & ReduxProps

class InspirationsTab extends React.Component<Props> {

  renderTemplates() {
    return this.props.templates.map((template, index, arr) => 
              <InspirationsCard
              key={`${template.id}`}
              template={template}
          />
      );
  }

  render() {

    const { classes } = this.props;

    return (
        <div id='inspirations-tab'>
            <Typography className={classes.root} component="h1" variant="h4" gutterBottom>Inspirations</Typography>
            {this.renderTemplates()}
        </div>
    );
  }

}

// Type checking redux

const mapStateToProps = (state: RootState) => {
  return {
      templates: state.templates
  }
}

const connector = connect(mapStateToProps, {})

type ReduxProps = ConnectedProps<typeof connector>

export default connector(withStyles(styles)(InspirationsTab));