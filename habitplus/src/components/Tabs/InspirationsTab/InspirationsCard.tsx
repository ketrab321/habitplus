import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DeleteIcon from '@material-ui/icons/Delete';
import IconHelper from '../../IconHelper';
import PublicIcon from '@material-ui/icons/Public';
import { Box, Button, CardActions } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import { connect } from 'react-redux';
import { TemplateType } from '../../../reducers/TemplatesReducers';
import { useTemplate, likeTemplate, unlikeTemplate, removeTemplate, setTemplateType } from '../../../actions/templateActions';
import Add from '@material-ui/icons/Add';
import ExposurePlus1Icon from '@material-ui/icons/ExposurePlus1';
import firebase from "firebase/app";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 10,
    },
    content: {
      paddingBottom: 5,
    },
    description: {
      marginBottom: 10,
    },
    templateActions: {
      justifyContent: 'center',
      fontSize: 18,
      paddingRight: 2,
      paddingLeft: 2,
      paddingTop: 10,
      paddingBottom: 10
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
  }),
);

type PropsType = {
    template: TemplateType,
    useTemplate: (habit: TemplateType) => void,
    likeTemplate: (habit: TemplateType) => void
    unlikeTemplate: (habit: TemplateType) => void
    removeTemplate: (habit: TemplateType) => void
    setTemplateType: (habit: TemplateType, newType: 'public' | 'private') => void
}


function InspirationsCard(props: PropsType) {
    const myId = firebase.auth().currentUser!.uid!;
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [isTemplateLiked, setTemplateLiked] = React.useState(props.template.voted.split(',').includes(myId));
    const [isTemplatePublic, setPublic] = React.useState(props.template.type === 'public');
    const isThisMyTemplate = myId === props.template.authorId;

    const handleExpandClick = () => {
      setExpanded(!expanded);
    };
  
    const useTemplate = () => {
      props.useTemplate(props.template);
    }
    const likeTemplate = () => {
      if (isTemplateLiked)
        props.unlikeTemplate(props.template);
      else
        props.likeTemplate(props.template);
        setTemplateLiked(!isTemplateLiked);
    }

    const removeTemplate = () => {
      props.removeTemplate(props.template);
    }

    const publishTemplate = () => {
      if (isTemplatePublic)
        props.setTemplateType(props.template, 'private');
      else
        props.setTemplateType(props.template, 'public');
      setPublic(!isTemplatePublic);
    }

    return (
      <Card className={classes.root} raised>
        <CardHeader
          avatar={
            <Avatar style={{
                "backgroundColor": props.template.icon.color,
                "color": props.template.icon.textColor
            }}>
              <IconHelper iconType={props.template.icon.type} />
            </Avatar>
          }
          action={
              <IconButton
                  className={clsx(classes.expand, {
                  [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  aria-label="show more"
              >
                  <ExpandMoreIcon />
              </IconButton>
          }
          onClick={handleExpandClick}
          title={props.template.title}
          subheader={`${props.template.authorName} (+${props.template.likes})`}
        />
        <Collapse in={expanded} timeout="auto">
          <CardContent className={classes.content}>
  
            <Box className={classes.description}>
              <Typography>
                <NewlineText text={props.template.description} />
              </Typography>
            </Box>
  
          </CardContent>
          <Divider />
          <CardActions className={classes.templateActions}>
  
          <Button startIcon={<Add />} variant='outlined' onClick={useTemplate}>Use</Button>
          {isThisMyTemplate ? <Button startIcon={<DeleteIcon />} variant='outlined' onClick={removeTemplate}>Remove</Button> : null}
          {isThisMyTemplate ? 
            <Button 
              startIcon={<PublicIcon />} 
              variant='outlined' 
              color={isTemplatePublic ? 'primary' : 'default'}
              onClick={publishTemplate}
            >Public</Button> : null}
          <Button 
            startIcon={<ExposurePlus1Icon />} 
            variant='outlined' 
            color={isTemplateLiked ? 'primary' : 'default'}
            onClick={likeTemplate}
          >
            Like
          </Button>
  
          </CardActions>
        </Collapse>
      </Card>
    );
  }
  
  function NewlineText(props: any) {
    const text = props.text;
    return text.split('\n').map((str: any, idx: number) => <span key={idx}>{str}<br /></span>);
  }
  
  
  const connector = connect(undefined, {
    useTemplate, likeTemplate, unlikeTemplate, removeTemplate, setTemplateType
  })
  
  export default connector(InspirationsCard);
