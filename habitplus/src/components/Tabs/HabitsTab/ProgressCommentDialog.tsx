import { Box, Button, Chip, Dialog, DialogActions, DialogContent, DialogTitle, Divider, TextareaAutosize, Typography } from '@material-ui/core';
import { Theme, createStyles, WithStyles, withStyles } from '@material-ui/core/styles';
import React from 'react';

import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import { HabitProgressComment } from '../../../reducers/HabitsReducers';


const styles = (theme: Theme) => createStyles({
  divider: {
    marginTop: "10px",
    marginBottom: "10px"
  },
  typografy: {
    marginBottom: "10px"
  },
  textArea: {
    width: "100%",
    backgroundColor: "rgba(0,0,0,0)",
    color: "white",
    fontFamily: "Roboto"
  },
  tag: {
    marginLeft: 0,
    marginRight: 6,
    marginTop: 6,
    marginBottom: 6,
  }
})


type Props = {
  isOpened: boolean,
  close: ()=>void,
  confirm: (comment: HabitProgressComment)=>void,
  title: string,
  defaultValue?: string
} & WithStyles<typeof styles>





class ProgressCommentDialog extends React.Component<Props, HabitProgressComment> {
    state: HabitProgressComment = {
        id: 'placeholder',
        type: "success",
        time: new Date(Date.now()).toISOString(),
        comment: this.props.defaultValue ? this.props.defaultValue : "Comment",
        tags: []
    }

  setComment(event: React.ChangeEvent<HTMLTextAreaElement>) {
    this.setState( {comment: event.target.value});
  }

  setType(type: "success"|"fail") {
      return () => {
          this.setState({type: type})
      }
  }

  renderTypeButtons(){
    const [successColor, failColor] = this.state.type === "success" ? ["primary", "default"] : ["default", "secondary"];
    return (
        <React.Fragment>
            <Chip 
                className={this.props.classes.tag} 
                color={successColor as ("primary" | "default")} 
                icon={<SentimentSatisfiedAltIcon/>} 
                label="Success"
                onClick={this.setType("success")}
            />
            <Chip 
                className={this.props.classes.tag} 
                color={failColor as ("secondary" | "default")} 
                icon={<SentimentVeryDissatisfiedIcon/>} 
                label="Fail"
                onClick={this.setType("fail")}
            />
        </React.Fragment>
    )
  }



  render(){
    const classes = this.props.classes;

    return (
      <Box>
        <Dialog
            open={this.props.isOpened}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{this.props.title}</DialogTitle>
            <DialogContent>
                <Typography className={classes.typografy} variant="subtitle2">
                  Here write your thoughts:
                </Typography>
                <TextareaAutosize id="comment" className={classes.textArea} rowsMin={3} value={this.state.comment} onChange={this.setComment.bind(this)}/>
                <Divider className={classes.divider}/>

                {this.renderTypeButtons()}
                <Typography className={classes.typografy} variant="subtitle2">
                  Choose if your today progress was a success or a fail. 
                  Remember, that you are allowed to have a bad day once in a while. 
                  You are okay the way you are. Just wake up tommorow with new determination :)
                </Typography>
            </DialogContent>
            <DialogActions>
                <Button onClick={this.props.close} color="secondary">
                    Go back
                </Button>
                <Button color="primary" autoFocus
                 onClick={()=>{
                    this.props.confirm(this.state)
                    this.props.close();
                 }}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
      </Box>
    );
  }
  
}

// Type checking redux




export default withStyles(styles)(ProgressCommentDialog);