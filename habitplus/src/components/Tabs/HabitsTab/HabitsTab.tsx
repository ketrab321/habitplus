import {  Box, Button } from "@material-ui/core"
import HabitsCard from './HabitsCard/HabitsCard';
import Add from '@material-ui/icons/Add';
import { connect, ConnectedProps } from 'react-redux';
import { addHabit, updateHabit, removeHabit, saveHabitAsTemplate } from '../../../actions';
import HabitDialog from "./HabitDialog";
import { useState } from "react";
import { RootState } from "../../../store";
import { HabitType } from "../../../reducers/HabitsReducers";


function HabitsTab(props: ReduxProps) {
    const [habitDialogOpen, setHabitDialogOpen] = useState(false);
    const [updateHabit, setUpdatedHabit] = useState<HabitType | undefined>(undefined);
    const openHabitDialog = (habit: HabitType | undefined) => {
        setUpdatedHabit(habit);
        setHabitDialogOpen(true);
    };
    const closeHabitDialog = () => {
        setUpdatedHabit(undefined);
        setHabitDialogOpen(false)
    };

    const confirm = (habit: HabitType) => {
        if(updateHabit){
            props.updateHabit(habit, updateHabit.id);
        }
        else{
            props.addHabit(habit);
        }
    }

    const removeHabit = (habit: HabitType) => {
        props.removeHabit(habit.id);
    }

    return (
        <Box id='habits-tab'>
            <Button startIcon={<Add />} variant='outlined' color='primary' onClick={()=>{openHabitDialog(undefined)}}>Add Habit</Button>
            {
                habitDialogOpen ? (<HabitDialog 
                    isOpened={habitDialogOpen} 
                    close={closeHabitDialog} 
                    defaultValue={updateHabit} 
                    confirm={confirm}/>) : null
            }
            {
                props.habits.map((habit, index, arr) => 
                    <HabitsCard
                        key={`habit-${index}`}
                        habit={habit}
                        update={(habit: HabitType)=>{openHabitDialog(habit)}}
                        remove={removeHabit}
                        saveAsTemplate={(habit: HabitType) => props.saveHabitAsTemplate(habit)}
                    />
                )
            }
        </Box>
    )
}


// Type checking redux

const mapStateToProps = (state: RootState) => {
    return {
        habits: state.habits
    }
}

const mapActions = {
    addHabit,
    updateHabit,
    removeHabit,
    saveHabitAsTemplate
}

const connector = connect(mapStateToProps, mapActions)

type ReduxProps = ConnectedProps<typeof connector>

export default connector(HabitsTab);