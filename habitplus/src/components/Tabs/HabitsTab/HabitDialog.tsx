import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, TextareaAutosize, TextField, Typography } from '@material-ui/core';
import { Theme, createStyles, WithStyles, withStyles } from '@material-ui/core/styles';
import React from 'react';
import IconPicker from '../../IconPicker';
import { CirclePicker } from 'react-color';
import { HabitType } from '../../../reducers/HabitsReducers';


const styles = (theme: Theme) => createStyles({
  divider: {
    marginTop: "10px",
    marginBottom: "10px"
  },
  typografy: {
    marginBottom: "10px"
  },
  textField: {
    width: "100%"
  },
  textArea: {
    width: "100%",
    backgroundColor: "rgba(0,0,0,0)",
    color: "white",
    fontFamily: "Roboto"
  }
})


type Props = {
  isOpened: boolean,
  close: ()=>void,
  confirm: (habit: HabitType) => void,
  defaultValue: HabitType | undefined
} & WithStyles<typeof styles>




class HabitDialog extends React.Component<Props, HabitType>    {
  constructor(props: Props){
    super(props);
    if(props.defaultValue){
      this.state = props.defaultValue;
    }
    else{
      this.state = {
        id: 'placeholder',
        title: "Habit title",
        description: "Habit description",
        icon:{
          type: 'help',
          color: '#4caf50',
          textColor: '#000',
        },
        startTime: new Date(Date.now()).toISOString(),
        history: []
      }
    }
  }
  

  setTitle(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState( {title: event.target.value});
  }

  setDescription(event: React.ChangeEvent<HTMLTextAreaElement>) {
    this.setState( {description: event.target.value});
  }

  setIcon(type: string) {
    this.setState({
      icon: {
        ...this.state.icon,
        type: type
      }
    })
  }

  handleIconBackgroundColorChange (color: {hex: string}, event: any) {
    this.setState({
      icon: {
        ...this.state.icon,
        color: color.hex
      }
    });
  };

  handleIconFontColorChange (color: {hex: string}, event: any) {
    this.setState({
      icon: {
        ...this.state.icon,
        textColor: color.hex
      }
    });
  };


  render(){
    const classes = this.props.classes;

    return (
      <Box>
        <Dialog
            open={this.props.isOpened}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">Create new habit</DialogTitle>
            <DialogContent>
                <Typography className={classes.typografy} variant="subtitle2">
                  Choose your habit title:
                </Typography>
                <TextField id="habittitle" className={classes.textField} label="Title" variant="outlined" value={this.state.title} onChange={this.setTitle.bind(this)}/>
                <Divider className={classes.divider}/>

                <Typography className={classes.typografy} variant="subtitle2">
                  Choose your habit description:
                </Typography>
                <TextareaAutosize id="habitdescription" className={classes.textArea} rowsMin={3} value={this.state.description} onChange={this.setDescription.bind(this)}/>
                <Divider className={classes.divider}/>

                <Typography className={classes.typografy} variant="subtitle2">
                  Choose your habit icon:
                </Typography>
                <IconPicker iconType={this.state.icon.type} color={this.state.icon.color} textColor={this.state.icon.textColor} onSelectIcon={this.setIcon.bind(this)}/>
                <Divider className={classes.divider}/>

                <Typography className={classes.typografy} variant="subtitle2">
                  Choose icon background color:
                </Typography>
                <CirclePicker color={this.state.icon.color} onChangeComplete={this.handleIconBackgroundColorChange.bind(this)} 
                  colors={["#000000", "#ffffff", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39", "#ffeb3b", "#ffc107", "#ff9800", "#ff5722", "#795548", "#607d8b"]}
                />
                <Divider className={classes.divider}/>

                <Typography className={classes.typografy} variant="subtitle2">
                  Choose icon font color:
                </Typography>
                <CirclePicker color={this.state.icon.textColor} onChangeComplete={this.handleIconFontColorChange.bind(this)} 
                  colors={["#000000", "#ffffff", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39", "#ffeb3b", "#ffc107", "#ff9800", "#ff5722", "#795548", "#607d8b"]}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={this.props.close} color="secondary">
                    Go back
                </Button>
                <Button color="primary" autoFocus
                 onClick={()=>{
                  this.props.confirm(this.state)
                  this.props.close();
                 }}>
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
      </Box>
    );
  }
  
}


export default withStyles(styles)(HabitDialog);