import { render, fireEvent, screen, queryByAttribute } from '../../../test-utils';
import HabitsTab from './HabitsTab';
import { HabitsType } from '../../../reducers/HabitsReducers';

const mockHabits: HabitsType = [
    {
        id: 'bruh',
        title: "Give up smoking",
        description: "I want to give up smoking",
        icon: {
            type: 'smoking',
            color: 'blue',
            textColor: 'white'
        },
        startTime: '2020-03-15T12:45:55',
        history: [
            {
                id: '0',
                type: 'fail',
                time: '2020-05-12T15:46:09',
                comment: 'I failed again.',
                tags: [ 'Too_much_coffee', 'Problems_at_work' ]
            },
            {
                id: '1',
                type: 'success',
                time: '2020-05-07T15:46:09',
                comment: 'Swimming helps get my mind of my addiction.',
                tags: [ 'Swimming' ]
            },
            {
                id: '2',
                type: 'fail',
                time: '2020-04-12T15:46:09',
                comment: 'I failed again.',
                tags: [ 'Too_much_coffee', 'Problems_at_work' ]
            }
        ]
    },
    {
        id: 'bruh',
        title: "Save money for vacation",
        description: "I want to go on a trip to Bieszczady",
        icon: {
            type: 'beach',
            color: 'yellow',
            textColor: 'black'
        },
        startTime: '2020-01-25T12:45:55',
        history: [
            {
                id: '3',
                type: 'fail',
                time: '2020-02-12T15:46:09',
                comment: 'I failed again.',
                tags: [ 'Problems_at_work', 'Spending_money_on_child'  ]
            }
        ]
    }
]

const getById = queryByAttribute.bind(null, 'id');

describe("Renders HabitsTab with initial state, and checks if it shows habits correctly", () => {
    var dom: any = undefined;
    beforeEach(()=>{
        dom = render(<HabitsTab />, { initialState: { habits: mockHabits } });
    })

    it("Should render two habit cards", ()=>{
        expect(dom.getByText("Give up smoking")).toBeInTheDocument();
        expect(dom.getByText("Save money for vacation")).toBeInTheDocument();

    })

    it("Should expand after clicking header", ()=> {
        let header = dom.getByText("Give up smoking");
        expect(dom.queryByText("I want to give up smoking")).not.toBeVisible();

        fireEvent.click(header);

        expect(dom.queryByText("I want to give up smoking")).toBeVisible();
    })

    it("Should open new habit dialog after clicking ADD HABIT button", ()=> {
        let button = dom.getByText("Add Habit");
        expect(dom.queryByText("Create new habit")).toBeNull();

        fireEvent.click(button);

        expect(dom.queryByText("Create new habit")).not.toBeNull();
    })

    // it("Should add new habit with provided data", ()=> {
    //     // Check if dialog opens
    //     let addHabitButton = dom.getByText("Add Habit");
    //     expect(dom.queryByText("Create new habit")).toBeNull();
    //     fireEvent.click(addHabitButton);
    //     expect(dom.queryByText("Create new habit")).not.toBeNull();

    //     // Set new habit data
    //     const habitTitle = dom.queryByText("Habit title");
    //     const habitDescription = dom.queryByText("Habit description");

    //     console.log(habitTitle);
        
    //     fireEvent.change(habitTitle, {target: {value: 'Title the title'}});
    //     fireEvent.change(habitDescription, {target: {value: 'Description the description'}});

    //     // Click confirm
    //     let confirmButton = dom.getByText("Confirm");
    //     fireEvent.click(confirmButton);

    //     // See if the new habit shows up
    //     const header = dom.queryByText("Title the title");
    //     expect(header).not.toBeNull();
    //     expect(header).toBeInTheDocument();

    //     // Check also the description
    //     fireEvent.click(header);
    //     expect(dom.queryByText("Description the description")).not.toBeNull();
    // })
})