import { Box, Button, Chip, Dialog, DialogActions, DialogContent, DialogTitle, Divider, TextField, Typography } from '@material-ui/core';
import { Theme, createStyles, WithStyles, withStyles } from '@material-ui/core/styles';
import React from 'react';
import { TagType } from '../../../reducers/TagsReducers';
import IconPicker from '../../IconPicker';
import IconHelper from '../../IconHelper';


const styles = (theme: Theme) => createStyles({
  divider: {
    marginTop: "10px",
    marginBottom: "10px"
  },
  typografy: {
    marginBottom: "10px"
  },
  textField: {
    width: "100%"
  },
  tag: {
    marginLeft: 0,
    marginRight: 6,
    marginTop: 6,
    marginBottom: 6,
  }
})


type Props = {
  isOpened: boolean,
  close: (tag: TagType)=>void,
  confirm: (tag: TagType)=>void,
  tag: TagType
} & WithStyles<typeof styles>





class EditTagDialog extends React.Component<Props, TagType> {
    state: TagType = {
        id: "placeholder",
        label: "Label",
        icon: "help"
    }

    componentDidMount(){
        this.setState(this.props.tag);
    }

    setLabel(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState( {label: event.target.value});
    }

    setIcon(type: string) {
        this.setState({
          icon: type
        });
    }


  render(){
    const classes = this.props.classes;

    return (
      <Box>
        <Dialog
            open={this.props.isOpened}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">Comment your progress</DialogTitle>
            <DialogContent>
                <Chip 
                    key={`tag-${this.state.id}`} 
                    className={this.props.classes.tag} 
                    variant='outlined' 
                    icon={<IconHelper iconType={this.state.icon}/>}
                    label={this.state.label}
                />
                <Typography className={classes.typografy} variant="subtitle2">
                  Choose tag label:
                </Typography>
                <TextField 
                    id="taglabel" 
                    className={classes.textField} 
                    label="Tag Label" 
                    variant="outlined"
                    value={this.state.label} 
                    onChange={this.setLabel.bind(this)}/>
                <Divider className={classes.divider}/>

                <Typography className={classes.typografy} variant="subtitle2">
                  Choose tag icon:
                </Typography>
                <IconPicker  iconType={this.state.icon} onSelectIcon={this.setIcon.bind(this)}/>
                
            </DialogContent>
            <DialogActions>
                <Button onClick={()=>{this.props.close(this.state)}} color="secondary">
                    Go back
                </Button>
                <Button color="primary" autoFocus
                 onClick={()=>{
                    this.props.confirm(this.state);
                 }}>
                    Edit tag
                </Button>
            </DialogActions>
        </Dialog>
      </Box>
    );
  }
  
}

export default withStyles(styles)(EditTagDialog);