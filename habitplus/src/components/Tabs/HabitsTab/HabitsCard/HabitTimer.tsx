import { Box, createStyles, makeStyles, Theme } from '@material-ui/core';
import React from 'react';
import ProgressCircle from './ProgressCircle';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    progress: {
      marginTop: 10,
    }
  }),
);


export function HabitTimer(props: {startTime: string}) {
    const classes = useStyles();

    const [currentTime, setCurrentTime] = React.useState(new Date())

    React.useEffect(() => {
        const timer = setInterval(() => {
        setCurrentTime(new Date());
        }, 1000);
        return () => {
        clearInterval(timer);
        };
    }, []);
    
    // If no icon is specified, set first title letter as icon
    const start = new Date(props.startTime);

    const days =  Math.floor(Math.abs(currentTime.getTime() - start.getTime()) / (24*36e5));
    const hours = Math.floor(Math.abs(currentTime.getTime() - start.getTime()) / 36e5 - days*24);
    const minutes = Math.floor(Math.abs(currentTime.getTime() - start.getTime()) / 6e4 - hours*60 - days*24*60);
    const seconds = Math.floor(Math.abs(currentTime.getTime() - start.getTime()) / 1e3 - hours*60*60 - minutes*60 - days*24*60*60);

    const hoursProgress = Math.floor(100*hours/24);
    const minutesProgress = Math.floor(100*minutes/60);
    const secondsProgress = Math.floor(100*seconds/60);
    return (
        <React.Fragment>
            <Box className={classes.progress}>
                <ProgressCircle value={100} label={`${days}d`}/>
                <ProgressCircle value={hoursProgress} label={`${hours}h`}/>
                <ProgressCircle value={minutesProgress} label={`${minutes}m`}/>
                <ProgressCircle value={secondsProgress} label={`${seconds}s`}/>
            </Box>
        </React.Fragment>
    )
}