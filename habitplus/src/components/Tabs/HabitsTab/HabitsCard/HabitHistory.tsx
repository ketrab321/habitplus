import React from 'react';
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { Paper, Typography, Chip, Divider, Button, Card, IconButton } from '@material-ui/core';

import IconHelper from '../../../IconHelper';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import AddIcon from '@material-ui/icons/Add';

import ProgressCommentDialog from '../ProgressCommentDialog';
import AddTagDialog from '../AddTagDialog';

import { TagType } from '../../../../reducers/TagsReducers';
import { createNewTag, editTag, addTagToProgressComment } from '../../../../actions/tagsActions';
import { addHabitProgressComment, editHabitProgressComment, removeHabitProgressComment } from '../../../../actions/habitsActions';

import { connect, ConnectedProps } from 'react-redux';
import { HabitProgressComment } from '../../../../reducers/HabitsReducers';
import EditTagDialog from '../EditTagDialog';
import { RootState } from '../../../../store';

const styles = (theme: Theme) => createStyles({
    root: {
      backgroundColor: theme.palette.background.paper,
      position: 'relative',
      overflow: 'auto',
      maxHeight: 300,
    },
    listItem: {
      width: '100%',
    },
    historyCard: {
      width: '100%',
      padding: 10,
      marginRight: 10,
      marginLeft: 10
    },
    progressButtonContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
    },
    progressButton: {
      width: '100%',
    },
    comment: {
      marginBottom: 2,
      paddingLeft: 7,
      paddingRight: 7,
      borderLeft: 'solid',
    },
    options: {
      marginTop: 5,
      alignItems: "flex-end",
      display: "flex",
      justifyContent: "flex-end"
    },
    tag: {
      marginLeft: 0,
      marginRight: 4,
      marginTop: 6,
    }
  });

type Props = {
  habitId: string,
  history: Array<HabitProgressComment>
} & WithStyles<typeof styles> & ReduxProps;

class HabitHistory extends React.Component<Props> {
  state = {
    newProgressCommentDialogOpen: false,
    editProgressCommentDialogOpen: false,
    addTagDialogOpen: false,
    editTagDialogOpen: false,
    historyId: "placeholder",
    commentId: "",
    editedTag: {
      id: "placeholder",
      label: "Label",
      icon: "help"
    },
  }

  getTagById(tagId: string) {
    let tag = this.props.tags.find(t => t.id === tagId);
    if(tag) {
      return <Chip 
                key={`tag-${tagId}`} 
                className={this.props.classes.tag} 
                variant='outlined' 
                icon={<IconHelper iconType={tag.icon}/>}
                label={tag.label}
                onClick={()=>{this.openEditTagDialog(tag!)}}
              />;
    }
    return null;
  }



  openNewProgressCommentDialog() {
    this.setState({
      newProgressCommentDialogOpen: true
    })
  }

  closeNewProgressCommentDialog() {
    this.setState({
      newProgressCommentDialogOpen: false
    })
  }

  openEditProgressCommentDialog(commentId: string) {
    this.setState({
      commentId: commentId,
      editProgressCommentDialogOpen: true
    })
  }

  closeEditProgressCommentDialog() {
    this.setState({
      editProgressCommentDialogOpen: false
    })
  }

  openAddTagDialog(historyId: string) {
    this.setState({
      addTagDialogOpen: true,
      historyId: historyId
    })
  }

  closeAddTagDialog(selectedTag: TagType) {
    this.setState({
      addTagDialogOpen: false,
    })
  }

  confirmAddTagDialog(selectedTag: TagType) {
    let tag = this.props.tags.find(t => t.icon === selectedTag.icon && t.label === selectedTag.label);
    if(tag){
      // Add existing tag
      this.props.addTagToProgressComment(this.props.habitId, this.state.historyId, tag.id);
    }
    else {
      // Create tag, and then add tag
      this.props.createNewTag(selectedTag.label, selectedTag.icon, this.props.habitId, this.state.historyId);
    }
    this.setState({
      addTagDialogOpen: false
    })
  }

  openEditTagDialog(tag: TagType) {
    this.setState({
      editedTag: tag,
      editTagDialogOpen: true,
    })
  }

  confirmEditTagDialog(selectedTag: TagType) {
    this.props.editTag(selectedTag);
    this.setState({
      editTagDialogOpen: false
    })
  }

  closeEditTagDialog(selectedTag: TagType) {
    this.setState({
      editTagDialogOpen: false
    })
  }

  removeComment(commentId: string) {
    this.props.removeHabitProgressComment(this.props.habitId, commentId);
  }

  

  renderTags(tags: Array<string> | undefined){
    if(!tags){
      return null;
    }
    else {
      return (
        <React.Fragment>
          {tags.map((tagId) => this.getTagById(tagId))}
        </React.Fragment>
      )
    } 
  }

  renderComments() {
    const classes = this.props.classes;

    return this.props.history.map((item, index, arr) => {
      let typeTag = item.type === 'fail' 
        ? <Chip className={classes.tag} color='secondary' icon={<SentimentVeryDissatisfiedIcon/>} label="Fail"/>
        : <Chip className={classes.tag} color='primary' icon={<SentimentSatisfiedAltIcon/>} label="Success"/>
      return (
        <ListItem className={classes.listItem} key={`history-${index}`} disableGutters={true}>
          <Paper className={classes.historyCard} elevation={3} variant={'outlined'}>
            <Paper className={classes.comment} elevation={0} square>
              <Typography>
                  {item.comment}
              </Typography>
            </Paper>
             
            {typeTag}
            <Chip 
              className={classes.tag} 
              icon={<AccessTimeIcon/>} 
              label={item.time}
            />
            {
              this.renderTags(item.tags)
            }
            <Chip 
              className={classes.tag} 
              variant='outlined' 
              icon={<AddIcon/>} 
              label="Add" 
              onClick={()=>this.openAddTagDialog(item.id)}
            />

            <Paper className={classes.options}>
              <IconButton aria-label="edit comment" onClick={()=>{this.openEditProgressCommentDialog(item.id)}}>
                <EditIcon />
              </IconButton>

              <IconButton aria-label="delete" onClick={()=>{this.removeComment(item.id)}}>
                <DeleteIcon />
              </IconButton>
            </Paper>
          </Paper>
        </ListItem>
      )
    })
  }

  render(){
    const classes = this.props.classes;

    if(this.props.history.length === 0){
      return null;
    }
    
    return (
      <React.Fragment>
        <ProgressCommentDialog 
          isOpened={this.state.newProgressCommentDialogOpen} 
          close={this.closeNewProgressCommentDialog.bind(this)} 
          title={"Comment your progress"}
          confirm={(comment: HabitProgressComment) => {this.props.addHabitProgressComment(this.props.habitId, comment)}}
        />
        <ProgressCommentDialog 
          isOpened={this.state.editProgressCommentDialogOpen} 
          close={this.closeEditProgressCommentDialog.bind(this)} 
          title={"Edit progress comment"}
          confirm={(comment: HabitProgressComment) => {this.props.editHabitProgressComment(this.props.habitId, this.state.commentId, comment)}}
        />
        <Card variant={'outlined'}>
          <Paper className={classes.progressButtonContainer} elevation={0} square>
            <Button 
              className={classes.progressButton}
              onClick={this.openNewProgressCommentDialog.bind(this)} 
              variant={"outlined"} 
              startIcon={<AddIcon />} 
              color='default'
            >
              Add new progress step
            </Button>
          </Paper>
          <Divider />
          {
            this.state.addTagDialogOpen ? (<AddTagDialog 
              isOpened={this.state.addTagDialogOpen} 
              close={this.closeAddTagDialog.bind(this)} 
              confirm={this.confirmAddTagDialog.bind(this)}
              tags={this.props.tags}
            />) : null
          }
          {
            this.state.editTagDialogOpen ? (<EditTagDialog 
              isOpened={this.state.editTagDialogOpen} 
              close={this.closeEditTagDialog.bind(this)} 
              confirm={this.confirmEditTagDialog.bind(this)}
              tag={this.state.editedTag}
            />) : null
          }
         
          
          <List className={classes.root}>
            {this.renderComments()} 
          </List>
        </Card>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {
      tags: state.tags
  }
}

const mapActionsToProps = {
  createNewTag,
  editTag,
  addTagToProgressComment,
  addHabitProgressComment,
  editHabitProgressComment,
  removeHabitProgressComment
}

const connector = connect(mapStateToProps, mapActionsToProps)

type ReduxProps = ConnectedProps<typeof connector>

export default connector(withStyles(styles)(HabitHistory));