import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import IconHelper from '../../../IconHelper';
import { Box, CardActions } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import HabitHistory from './HabitHistory';

import { HabitType } from '../../../../reducers/HabitsReducers';
import { HabitTimer } from './HabitTimer';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 10,
    },
    content: {
      paddingBottom: 5,
    },
    description: {
      marginBottom: 10,
    },
    progressHistoryHeader: {
      fontSize: 18,
      paddingRight: 2,
      paddingLeft: 2,
      paddingTop: 10,
      paddingBottom: 10
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    stickRight: {
      marginLeft: "auto",
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
  }),
);

type PropsType = {
    habit: HabitType,
    saveAsTemplate: (habit: HabitType) => void,
    update: (habit: HabitType) => void,
    remove: (habit: HabitType) => void
}

function HabitsCard(props: PropsType) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [historyExpanded, setHistoryExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleHistoryExpandClick = () => {
    setHistoryExpanded(!historyExpanded)
  }
  const start = new Date(props.habit.startTime);


  return (
    <Card className={classes.root} raised>
      <CardHeader
        avatar={
          <Avatar style={{
              "backgroundColor": props.habit.icon.color,
              "color": props.habit.icon.textColor
          }}>
            <IconHelper iconType={props.habit.icon.type} />
          </Avatar>
        }
        action={
            <IconButton
                className={clsx(classes.expand, {
                [classes.expandOpen]: expanded,
                })}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
            >
                <ExpandMoreIcon />
            </IconButton>
        }
        onClick={handleExpandClick}
        title={props.habit.title}
        subheader={start.toLocaleString()}
      />
      <Collapse in={expanded} timeout="auto">
        <CardContent className={classes.content}>

          <Box className={classes.description}>
            <Typography>
              <NewlineText text={props.habit.description} />
            </Typography>
          </Box>

          <Divider />

          <p>Time you keeped your habit: </p>
          <HabitTimer startTime={props.habit.startTime}/>

          <Divider />

          <CardHeader
            className={classes.progressHistoryHeader}
            action={
                <IconButton
                    className={clsx(classes.expand, {
                    [classes.expandOpen]: historyExpanded,
                    })}
                    onClick={handleHistoryExpandClick}
                    aria-expanded={historyExpanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            }
            onClick={handleHistoryExpandClick}
            title={"Progress history:"}
            disableTypography
          />
          <Collapse in={historyExpanded} timeout="auto">
              <HabitHistory history={props.habit.history} habitId={props.habit.id}/>
          </Collapse>

        </CardContent>
        <Divider />
        <CardActions>

        <IconButton className={classes.stickRight} aria-label="save as template" onClick={()=>{props.saveAsTemplate(props.habit)}}>
            <SaveAltIcon />
          </IconButton>

          <IconButton className={classes.stickRight} aria-label="update habit" onClick={()=>{props.update(props.habit)}}>
            <EditIcon />
          </IconButton>

          <IconButton aria-label="delete" onClick={()=>{props.remove(props.habit)}}>
            <DeleteIcon />
          </IconButton> 

        </CardActions>
      </Collapse>
    </Card>
  );
}

function NewlineText(props: any) {
  const text = props.text;
  return text.split('\n').map((str: any, idx: number) => <span key={idx}>{str}<br /></span>);
}

export default HabitsCard;