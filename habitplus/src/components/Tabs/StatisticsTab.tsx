import { Card, CardContent, Typography } from '@material-ui/core';
import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, AreaChart, Area } from 'recharts';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis } from 'recharts';
import { createStyles, withStyles, WithStyles } from "@material-ui/core/styles";
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../../store';


const styles = (theme: any) => createStyles({
    root: {
        textAlign: "center"
    },
    card: {
        margin: theme.spacing(2),
        '& > h1': {
            padding: "0.5em"
        },
        '& p': {
            textAlign: "center"
        }
    },
});


type Props = WithStyles<typeof styles> & ReduxProps

class StatisticsTab extends React.Component<Props> {

    getDataDaily() {
        const history = this.props.habits.map(habit => habit.history).flat();
        const data = [
            {day: 'Mon', succ: 0, fail: 0},
            {day: 'Tue', succ: 0, fail: 0},
            {day: 'Wed', succ: 0, fail: 0},
            {day: 'Thu', succ: 0, fail: 0},
            {day: 'Fri', succ: 0, fail: 0},
            {day: 'Sat', succ: 0, fail: 0},
            {day: 'Sun', succ: 0, fail: 0},
        ];
        for(const item of history) {
            const dataIndex = (new Date(item.time).getDay() + 6) % 7;
            const succ = item.type === "success";
            data[dataIndex].succ += succ ? 1 : 0;
            data[dataIndex].fail += succ ? 0 : 1;
        }
        return data;
    }

    getDataHourly() {
        const history = this.props.habits.map(habit => habit.history).flat();
        const data = [
            {time: '0:00', succ: 0, fail: 0},   // 0:  0 -  3
            {time: '4:00', succ: 0, fail: 0},   // 1:  4 -  7
            {time: '8:00', succ: 0, fail: 0},   // 2:  8 - 11
            {time: '12:00', succ: 0, fail: 0},  // 3: 12 - 15
            {time: '16:00', succ: 0, fail: 0},  // 4: 16 - 19
            {time: '20:00', succ: 0, fail: 0},  // 5: 20 - 23
        ];
        for(const item of history) {
            const dataIndex = Math.trunc(new Date(item.time).getHours() / 4);
            const succ = item.type === "success";
            data[dataIndex].succ += succ ? 1 : 0;
            data[dataIndex].fail += succ ? 0 : 1;
        }
        return data;
    }

    getDataForHabits() {
        const data = this.props.habits.map(habit => { 
            return {
                habit: habit.title,
                succ: habit.history.reduce((total, curr) => curr.type === "success" ? total+1 : total, 0),
                fail: habit.history.reduce((total, curr) => curr.type === "fail" ? total+1 : total, 0),
            }
        });
        return data;
    }

    renderDailyPlot() {
        const { classes } = this.props;
        const dataDaily = this.getDataDaily();

        return (
            <Card className={classes.card} >
                <CardContent>
                    <Typography variant="h5" component="h2" gutterBottom>
                        Progress (daily)
                    </Typography>

                    <ResponsiveContainer width="100%" height={350}>
                        <BarChart
                            width={500}
                            height={300}
                            data={dataDaily}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="day" tick={{ fill: "#dadada" }} />
                            <YAxis tick={{ fill: "#dadada" }} />
                            <Tooltip
                                contentStyle={{ backgroundColor: "#303030" }}
                                labelStyle={{ color: "#dadada" }}
                                cursor={{ fill: '#303030', fillOpacity: 0.6, stroke: '#fff', strokeWidth: 2 }}
                            />
                            <Legend />
                            <Bar dataKey="succ" stackId="a" fill="#82ca9d" name="Successes" />
                            <Bar dataKey="fail" stackId="a" fill="#ff7575" name="Fails" />
                        </BarChart>
                    </ResponsiveContainer>
                </CardContent>
            </Card>
        );
    }

    renderHourlyPlot() {
        const { classes } = this.props;
        const dataHourly = this.getDataHourly();

        return (
            <Card className={classes.card} >
                <CardContent>
                    <Typography variant="h5" component="h2" gutterBottom>
                        Progress (hourly)
                    </Typography>

                    <ResponsiveContainer width="100%" height={350}>
                        <AreaChart
                            width={500}
                            height={400}
                            data={dataHourly}
                            stackOffset="expand"
                            margin={{
                                top: 10,
                                right: 30,
                                left: 0,
                                bottom: 0,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="time" tick={{ fill: "#dadada" }} />
                            <YAxis tickFormatter={num => `${(num * 100)}%`} tick={{ fill: "#dadada" }} />
                            <Tooltip
                                contentStyle={{ backgroundColor: "#303030" }}
                                labelStyle={{ color: "#dadada" }}
                                cursor={{ fill: '#303030', fillOpacity: 0.6, stroke: '#fff', strokeWidth: 2 }}
                            />
                            <Legend />
                            <Area type="monotone" dataKey="succ" stackId="1" stroke="#82ca9d" fill="#82ca9d" name="Successes" />
                            <Area type="monotone" dataKey="fail" stackId="1" stroke="#ff7575" fill="#ff7575" name="Fails" />
                        </AreaChart>
                    </ResponsiveContainer>
                </CardContent>
            </Card>
        );
    }

    renderRadarPlot() {
        const { classes } = this.props;
        const dataForHabits = this.getDataForHabits();

        if (dataForHabits.length < 3) {
            return null;
        }

        return (
            <Card className={classes.card} >
                <CardContent>
                    <Typography variant="h5" component="h2" gutterBottom>
                        Your activity
                    </Typography>

                    <ResponsiveContainer width="100%" height={350}>
                        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={dataForHabits}>
                            <PolarGrid />
                            <PolarAngleAxis dataKey="habit" tick={{ fill: "#dadada" }} />
                            <PolarRadiusAxis />
                            <Radar name="Successes" dataKey="succ" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.6} />
                            <Radar name="Fails" dataKey="fail" stroke="#ff7575" fill="#ff7575" fillOpacity={0.6} />
                            <Tooltip
                                contentStyle={{ backgroundColor: "#303030" }}
                                labelStyle={{ color: "#dadada" }}
                                cursor={{ fill: '#303030', fillOpacity: 0.6, stroke: '#fff', strokeWidth: 2 }}
                            />
                            <Legend />
                        </RadarChart>
                    </ResponsiveContainer>
                </CardContent>
            </Card>
        )
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root} id='statistics-tab'>
                <Typography component="h1" variant="h4" gutterBottom>Statistics Tab</Typography>
                {this.renderDailyPlot()}
                {this.renderHourlyPlot()}
                {this.renderRadarPlot()}
            </div>
        );
    }


}



const mapStateToProps = (state: RootState) => {
    return {
        habits: state.habits
    }
}

type ReduxProps = ConnectedProps<typeof connector>

const connector = connect(mapStateToProps);
export default connector(withStyles(styles)(StatisticsTab));