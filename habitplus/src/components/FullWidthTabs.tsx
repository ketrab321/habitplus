import React from 'react';
import SwipeableViews from 'react-swipeable-views';

// Material UI
import { makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import BarChartIcon from '@material-ui/icons/BarChart';
import FlareIcon from '@material-ui/icons/Flare';
import StarRateIcon from '@material-ui/icons/StarRate';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Box from '@material-ui/core/Box';
// Redux
import { connect } from 'react-redux';
import { changeTabIndex } from '../actions';
import HabitsTab from './Tabs/HabitsTab/HabitsTab';
import StatisticsTab from './Tabs/StatisticsTab';
import InspirationsTab from './Tabs/InspirationsTab/InspirationsTab';
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import ProfileTab from './Tabs/ProfileTab';

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
  className: string
}
  
function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3} style={{
          "flexGrow": 1
        }}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    height: "inherit",
    display: 'flex',
    flexDirection: 'column',
  },
  nav: {
    maxHeight: "72px",
  },
  tabs: {
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    flexGrow: 1,
  }

}));
  
type PropsType = {
  tabIndex: number,
  changeTabIndex: Function
}


function FullWidthTabs(props: PropsType) {
  const classes = useStyles();
  const theme = useTheme();

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    props.changeTabIndex(newValue)
  };

  const handleChangeIndex = (index: number) => {
      props.changeTabIndex(index)
  };

  return (
    <div className={classes.root}>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={props.tabIndex}
        onChangeIndex={handleChangeIndex}
        enableMouseEvents = {true}
        style={{
          "flexGrow": 1,
          "display": 'block',
        }}
        slideStyle={{
          "display": "flex",
          "flexGrow": 1,
        }}
      >
          <TabPanel className={classes.tabs} value={props.tabIndex} index={0} dir={theme.direction} >
            <ProfileTab/>
          </TabPanel>
          <TabPanel className={classes.tabs} value={props.tabIndex} index={1} dir={theme.direction} >
            <HabitsTab/>
          </TabPanel>
          <TabPanel className={classes.tabs} value={props.tabIndex} index={2} dir={theme.direction}>
            <StatisticsTab/>
          </TabPanel>
          <TabPanel className={classes.tabs} value={props.tabIndex} index={3} dir={theme.direction}>
            <InspirationsTab/>
          </TabPanel>
        
      </SwipeableViews>
      <BottomNavigation
          value={props.tabIndex}
          onChange={handleChange}
          showLabels
        >
          <BottomNavigationAction  icon={<AccountCircleIcon />} label="Profile" {...a11yProps(0)} />
          <BottomNavigationAction  icon={<FlareIcon />} label="My Habits" {...a11yProps(1)} />
          <BottomNavigationAction  icon={<BarChartIcon />} label="Statistics" {...a11yProps(2)} />
          <BottomNavigationAction  icon={<StarRateIcon />} label="Inspirations" {...a11yProps(3)} />
        </BottomNavigation>
    </div>
  );
}
  

const mapStateToProps = (state: any) => {
  return {
      tabIndex: state.tabIndex
  }
}

export default connect(mapStateToProps, { changeTabIndex })(FullWidthTabs);