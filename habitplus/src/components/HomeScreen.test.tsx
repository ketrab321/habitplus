import { render, fireEvent, screen, queryByAttribute } from '../test-utils'
import HomeScreen from './HomeScreen'

const getById = queryByAttribute.bind(null, 'id');


describe("Renders app with initial state and tests changing tabs with swipes and navbar", () => {
  
  it('Checks if App renders good, and displays tabs appropriatly', () => {
    const dom = render(<HomeScreen />, { initialState: { tabIndex: 2 } });

    expect(dom.getByText('Profile')).toBeInTheDocument();
    expect(dom.getByText('My Habits')).toBeInTheDocument();
    expect(dom.getByText('Statistics')).toBeInTheDocument();
    expect(dom.getByText('Inspirations')).toBeInTheDocument();

    expect(getById(dom.container, 'tab-0')).toBeInTheDocument();
    expect(getById(dom.container, 'tab-1')).toBeInTheDocument();
    expect(getById(dom.container, 'tab-2')).toBeInTheDocument();
    expect(getById(dom.container, 'tab-3')).toBeInTheDocument();

    expect(getById(dom.container, 'tab-0')).not.toHaveClass("Mui-selected");
    expect(getById(dom.container, 'tab-1')).not.toHaveClass("Mui-selected");
    expect(getById(dom.container, 'tab-2')).toHaveClass("Mui-selected");
    expect(getById(dom.container, 'tab-3')).not.toHaveClass("Mui-selected");

    expect(getById(dom.container, 'profile-tab')).toBeNull();
    expect(getById(dom.container, 'habits-tab')).toBeNull();
    expect(getById(dom.container, 'statistics-tab')).toBeInTheDocument();
    expect(getById(dom.container, 'inspirations-tab')).toBeNull();
  });

  it('Tests changing tabs using navbar', ()=> {
    const dom = render(<HomeScreen />, { initialState: { tabIndex: 1 } });

    const tab = getById(dom.container, 'tab-2')!;
    expect(tab).toBeInTheDocument();
    expect(getById(dom.container, 'tab-1')).toBeInTheDocument();
    expect(getById(dom.container, 'tab-2')).toBeInTheDocument();
    expect(getById(dom.container, 'tab-3')).toBeInTheDocument();

    expect(getById(dom.container, 'tab-0')).not.toHaveClass('Mui-selected');
    expect(getById(dom.container, 'tab-1')).toHaveClass('Mui-selected');
    expect(getById(dom.container, 'tab-2')).not.toHaveClass('Mui-selected');
    expect(getById(dom.container, 'tab-3')).not.toHaveClass('Mui-selected');

    fireEvent.click(tab);

    expect(getById(dom.container, 'tab-0')).not.toHaveClass('Mui-selected');
    expect(getById(dom.container, 'tab-1')).not.toHaveClass('Mui-selected');
    expect(getById(dom.container, 'tab-2')).toHaveClass('Mui-selected');
    expect(getById(dom.container, 'tab-3')).not.toHaveClass('Mui-selected');

    expect(getById(dom.container, 'profile-tab')).toBeNull();
    expect(getById(dom.container, 'habits-tab')).toBeNull();
    expect(getById(dom.container, 'statistics-tab')).toBeInTheDocument();
    expect(getById(dom.container, 'inspirations-tab')).toBeNull();
  });

})
