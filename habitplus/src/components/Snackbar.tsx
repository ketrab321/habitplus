import React from 'react';
import MuiSnackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { closeSnackbar } from '../actions';
import { connect } from 'react-redux';
import type {Color} from '@material-ui/lab/Alert';


type Props = {
    open: boolean,
    message: string,
    severity: Color
    closeSnackbar: Function
};

class Snackbar extends React.Component<Props> {

    render() {
        return (
            <MuiSnackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={this.props.open} 
                autoHideDuration={4000} 
                onClose={this.onCloseSnackbar}
            >
                <MuiAlert elevation={6} variant="filled" onClose={this.onCloseSnackbar} severity={this.props.severity}>
                    {this.props.message}
                </MuiAlert>
            </MuiSnackbar>
        );
    }

    onCloseSnackbar = (event?: React.SyntheticEvent, reason?: string) => {
        this.props.closeSnackbar();
    }

}

const mapStateToProps = (state: any) => {
    return {
        open: state.snackbar.open,
        message: state.snackbar.message,
        severity: state.snackbar.severity
    }
  }

const connector = connect(mapStateToProps, {closeSnackbar});
export default connector(Snackbar);
