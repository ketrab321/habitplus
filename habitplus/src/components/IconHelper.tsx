import AirlineSeatIndividualSuiteIcon from '@material-ui/icons/AirlineSeatIndividualSuite';
import AlarmOnIcon from '@material-ui/icons/AlarmOn';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Brightness3Icon from '@material-ui/icons/Brightness3';
import Brightness5Icon from '@material-ui/icons/Brightness5';
import BrushIcon from '@material-ui/icons/Brush';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import CakeIcon from '@material-ui/icons/Cake';
import ChildFriendlyIcon from '@material-ui/icons/ChildFriendly';
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike';
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import FreeBreakfastIcon from '@material-ui/icons/FreeBreakfast';
import GradeIcon from '@material-ui/icons/Grade';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import LaptopMacIcon from '@material-ui/icons/LaptopMac';
import LocalBarIcon from '@material-ui/icons/LocalBar';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import PetsIcon from '@material-ui/icons/Pets';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import PoolIcon from '@material-ui/icons/Pool';
import SchoolIcon from '@material-ui/icons/School';
import SmokingRoomsIcon from '@material-ui/icons/SmokingRooms';
import SportsEsportsIcon from '@material-ui/icons/SportsEsports';
import WeekendIcon from '@material-ui/icons/Weekend';
import { DefaultComponentProps } from '@material-ui/core/OverridableComponent';
import { SvgIconTypeMap } from '@material-ui/core';


export default function IconHelper(props: {iconType: string} & DefaultComponentProps<SvgIconTypeMap<{}, "svg">>){
    const {iconType, ...other} = props;
    switch(iconType){
        case "bed":{
            return <AirlineSeatIndividualSuiteIcon {...other} />;
        }
        case "alarm":{
            return <AlarmOnIcon {...other}/>;
        }
        case "beach":{
            return <BeachAccessIcon {...other}/>;
        }
        case "moon":{
            return <Brightness3Icon {...other}/>;
        }
        case "sun":{
            return <Brightness5Icon {...other}/>;
        }
        case "brush":{
            return <BrushIcon {...other}/>;
        }
        case "bubbles":{
            return <BubbleChartIcon {...other}/>;
        }
        case "work":{
            return <BusinessCenterIcon {...other}/>;
        }
        case "cake":{
            return <CakeIcon {...other}/>;
        }
        case "child": {
            return <ChildFriendlyIcon {...other}/>;
        }
        case "bike": {
            return <DirectionsBikeIcon {...other}/>;
        }
        case "run": {
            return <DirectionsRunIcon {...other}/>;
        }
        case "car": {
            return <DirectionsCarIcon {...other}/>;
        }
        case "trophy": {
            return <EmojiEventsIcon {...other}/>;
        }
        case "food": {
            return <FastfoodIcon {...other}/>;
        }
        case "heart": {
            return <FavoriteIcon {...other}/>;
        }
        case "fitness": {
            return <FitnessCenterIcon {...other}/>;
        }
        case "coffee": {
            return <FreeBreakfastIcon {...other}/>;
        }
        case "star": {
            return <GradeIcon {...other}/>;
        }
        case "help": {
            return <HelpOutlineIcon {...other}/>;
        }
        case "laptop": {
            return <LaptopMacIcon {...other}/>;
        }
        case "alcohol": {
            return <LocalBarIcon {...other}/>;
        }
        case "music": {
            return <MusicNoteIcon {...other}/>;
        }
        case "people": {
            return <PeopleOutlineIcon {...other}/>;
        }
        case "pet": {
            return <PetsIcon {...other}/>;
        }
        case "camera": {
            return <PhotoCameraIcon {...other}/>;
        }
        case "swimming": {
            return <PoolIcon {...other}/>;
        }
        case "learning": {
            return <SchoolIcon {...other}/>;
        }
        case "smoking": {
            return <SmokingRoomsIcon {...other}/>;
        }
        case "games": {
            return <SportsEsportsIcon {...other}/>;
        }
        case "weekend": {
            return <WeekendIcon {...other}/>;
        }
        default: {
            return null;
        }
    }
}