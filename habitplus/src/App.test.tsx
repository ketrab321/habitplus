import { render } from './test-utils'
import App from './App'


const firebaseMock = {
  auth: () => ({
    onAuthStateChanged: (_: any) => {}
  })
}

describe("Renders app with sign in screen", () => {
  
  it('Checks if loading screen appears', () => {
    const dom = render(<App firebase={firebaseMock} />, { initialState: {} });
    expect(dom.getByText('Logging in...')).toBeInTheDocument();
    
  });

})
