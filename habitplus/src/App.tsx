import React from "react";
import "./App.css";
import LoginScreen from "./components/LoginScreen";
import HomeScreen from "./components/HomeScreen";
import firebase from "firebase/app";

import { connect } from "react-redux";
import { signIn, signOut } from "./actions";

type Props = {
  firebase: firebase.app.App | any;
  userSignedIn?: boolean;
  signIn: Function;
  signOut: Function;
};

class App extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    props.firebase.auth().onAuthStateChanged((user: firebase.User) => {
      if (user) {
        this.props.signIn(user.uid);
      } else {
        this.props.signOut();
      }
    });
  }

  render() {
    if (this.props.userSignedIn === null) {
      return <div>Logging in...</div>;
    }
    return (
      <div className="App">
        {this.props.userSignedIn ? <HomeScreen /> : <LoginScreen />}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    userSignedIn: state.signedIn,
  };
};

export default connect(mapStateToProps, {
  signIn,
  signOut,
})(App);
