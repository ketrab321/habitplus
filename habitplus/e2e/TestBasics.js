import { Selector, Role } from 'testcafe';

fixture `Habit App`
    .page `https://ketrab321.gitlab.io/habitplus`
    .beforeEach( async t => {
        await t  // Login to test account
            .click(Selector('button').withText('Sign in with email'))
            .typeText('#ui-sign-in-email-input', 'test@example.com')
            .click(Selector('button').withText('NEXT'))
            .typeText('#ui-sign-in-password-input', 'abc123')
            .click(Selector('button').withText('SIGN IN'));
    })
    .afterEach( async t => {
        await t // Logout from test account
            .click("#tab-0")
            .click(Selector('button').withText('SIGN OUT'));

        await t.expect(Selector('button').withText('Sign in with Google').exists).ok();
    });


test('User Profile', async t => {
    await t.expect(Selector('input#username').value).eql('Tester');
    await t.expect(Selector('input#email').value).eql('test@example.com');
});
