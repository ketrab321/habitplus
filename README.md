# HabitPlus

The goal of this project is to create an app for developing new habits. The project incorporates such methodologies as RWD (responsive web design), PWA (progresive web app) and SPA (single page application). It is created in React and Node.js.

For more info look in the project Wiki page.